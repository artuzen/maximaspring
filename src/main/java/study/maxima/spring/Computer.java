package study.maxima.spring;

import java.util.List;

public class Computer {

    private CPU cpu;
    private List<RAM> ramList;

    public Computer(CPU cpu) {
        this.cpu = cpu;
    }

    public Computer(CPU cpu, List<RAM> ramList) {
        this.cpu = cpu;
        this.ramList = ramList;
    }

    public CPU getCpu() {
        return cpu;
    }

     public void setCpu(CPU cpu) {
        this.cpu = cpu;
    }

    public List<RAM> getRamList() {
        return ramList;
    }

    public void setRamList(List<RAM> ramList) {
        this.ramList = ramList;
    }
}
