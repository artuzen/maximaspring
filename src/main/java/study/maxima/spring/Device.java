package study.maxima.spring;

public interface Device {

    default String getName() {
        return this.getClass().getSimpleName();
    }
}
