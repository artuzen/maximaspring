package study.maxima.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;
import java.util.stream.Collectors;

public class App {

    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("beans.xml");
        System.out.println(Arrays.toString(context.getBeanDefinitionNames()));

       Computer computer = (Computer)context.getBean("myComputer");
       System.out.println("CPU: " + computer.getCpu().getName());
       System.out.println("RAM: " + computer.getRamList()
               .stream()
               .map(Device::getName)
               .collect(Collectors.joining(", ")));

    }


}
